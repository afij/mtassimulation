

#ifndef MTASPhysicsList_h
#define MTASPhysicsList_h 1

#include "G4VUserPhysicsList.hh"
#include "globals.hh"

class MTASPhysicsList: public G4VUserPhysicsList
{
  public:
    MTASPhysicsList();
    ~MTASPhysicsList();

  protected:
	virtual void ConstructParticle();
	virtual void ConstructProcess();

    virtual void SetCuts();

    G4int OpVerbLevel;

// Particles are made here
	void ConstructBosons();
	void ConstructLeptons();
	void ConstructMesons();
	void ConstructBaryons();
// Processes
	virtual void ConstructGeneral();
	virtual void ConstructEM();
        virtual void ConstructHad();
        virtual void ConstructOp();
};

#endif // MTASPhysicsList_h

