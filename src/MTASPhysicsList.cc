//
// ********************************************************************
// * DISCLAIMER                                                       *
// *                                                                  *
// * The following disclaimer summarizes all the specific disclaimers *
// * of contributors to this software. The specific disclaimers,which *
// * govern, are listed with their locations in:                      *
// *   http://cern.ch/geant4/license                                  *
// *                                                                  *
// * Neither the authors of this software system, nor their employing *
// * institutes,nor the agencies providing financial support for this *
// * work  make  any representation or  warranty, express or implied, *
// * regarding  this  software system or assume any liability for its *
// * use.                                                             *
// *                                                                  *
// * This  code  implementation is the  intellectual property  of the *
// * GEANT4 collaboration.                                            *
// * By copying,  distributing  or modifying the Program (or any work *
// * based  on  the Program)  you indicate  your  acceptance of  this *
// * statement, and all its terms.                                    *
// ********************************************************************
//

#include "MTASPhysicsList.hh"

#include "globals.hh"

#include "G4ParticleDefinition.hh"
#include "G4ParticleWithCuts.hh"
#include "G4ProcessManager.hh"
#include "G4ProcessVector.hh"
#include "G4ParticleTypes.hh"
#include "G4ParticleTable.hh"
#include "G4ShortLivedConstructor.hh"
#include "G4Material.hh"
#include "G4MaterialTable.hh"
#include "G4ios.hh"
//#include <iomanip>   

//#include "MTASGeneralPhysics.hh"
//#include "MTASLeptonPhysics.hh"
//#include "MTASHadronPhysics.hh"
//#include "MTASIonPhysics.hh"

#include "G4Decay.hh"
#include "G4RadioactiveDecay.hh"
#include "G4IonTable.hh"
#include "G4Ions.hh"
//gamma:
#include "G4PhotoElectricEffect.hh"
#include "G4LivermorePhotoElectricModel.hh"

#include "G4ComptonScattering.hh"
#include "G4LivermoreComptonModel.hh"

#include "G4GammaConversion.hh"
#include "G4LivermoreGammaConversionModel.hh"

#include "G4RayleighScattering.hh" 
#include "G4LivermoreRayleighModel.hh"

//#include "G4LowEnergyCompton.hh"
//#include "G4GammaConversion.hh"
//#include "G4LowEnergyPhotoElectric.hh"
#include "G4eIonisation.hh"
#include "G4eBremsstrahlung.hh"
#include "G4eplusAnnihilation.hh"
#include "G4eMultipleScattering.hh"
#include "G4hLowEnergyIonisation.hh"

//Optical Physics
//#include "G4Cerenkov.hh"
//#include "G4Scintillation.hh"
#include "G4OpAbsorption.hh"
#include "G4OpRayleigh.hh"
#include "G4OpBoundaryProcess.hh"

//#include "GLG4Scint.hh"
//#include "GLG4OpAttenuation.hh" ADD LATER

MTASPhysicsList::MTASPhysicsList(): G4VUserPhysicsList()
{
	defaultCutValue = 1.0*mm;
	OpVerbLevel = 0;
}

MTASPhysicsList::~MTASPhysicsList()
{// I do not have to delete the phsyics lists made above? Nope.
}

void MTASPhysicsList::ConstructParticle()
{
  ConstructBosons();
  ConstructLeptons();
  ConstructMesons();
  ConstructBaryons();
}

void MTASPhysicsList::ConstructProcess()
{
  AddTransportation();
  ConstructEM();
  ConstructOp();   
  ConstructHad(); //added SNL for neutron interactions
  ConstructGeneral();
}

void MTASPhysicsList::SetCuts()
{
//  " G4VUserPhysicsList::SetCutsWithDefault" method sets
//   the default cut value for all particle types
	defaultCutValue = 0.000001 * mm;
	SetCutsWithDefault();
}

void MTASPhysicsList::ConstructBosons()
{
  // pseudo-particles
  G4Geantino::GeantinoDefinition();
  G4ChargedGeantino::ChargedGeantinoDefinition();

  // gamma
  G4Gamma::GammaDefinition();
  // optical photon
  G4OpticalPhoton::OpticalPhotonDefinition();
}

void MTASPhysicsList::ConstructLeptons()
{
  // leptons
  G4Electron::ElectronDefinition();
  G4Positron::PositronDefinition();
  G4NeutrinoE::NeutrinoEDefinition();
  G4AntiNeutrinoE::AntiNeutrinoEDefinition();

  G4MuonMinus::MuonMinusDefinition();
  G4MuonPlus::MuonPlusDefinition();
  G4NeutrinoMu::NeutrinoMuDefinition();
  G4AntiNeutrinoMu::AntiNeutrinoMuDefinition();
}

void MTASPhysicsList::ConstructMesons()
{
}

#include "G4MesonConstructor.hh"
#include "G4BaryonConstructor.hh"
#include "G4IonConstructor.hh"

void MTASPhysicsList::ConstructBaryons()
{
// added SNL 3/12/07  I belive that the proton, anti-proton, neutron
// and anti neutron models for hadronic interactions introduce 
// particles that are not explicitly declared here.  With the 
// constructors below the program will fail on attempting to
// perform the construct general loop.

    //G4Proton::ProtonDefinition();
    ///G4AntiProton::AntiProtonDefinition();
    //G4Neutron::NeutronDefinition();
    //G4AntiNeutron::AntiNeutronDefinition();
 //  mesons
  G4MesonConstructor mConstructor;
  mConstructor.ConstructParticle();
 //  baryon
  G4BaryonConstructor bConstructor;
  bConstructor.ConstructParticle();
 //  mesons
  G4IonConstructor iConstructor;
  iConstructor.ConstructParticle();

}

void MTASPhysicsList::ConstructEM()
{
	theParticleIterator->reset();

	while( (*theParticleIterator)() )
	{
      G4ParticleDefinition* particle = theParticleIterator->value();
      G4ProcessManager* pManager = particle->GetProcessManager();
      G4String particleName = particle->GetParticleName();

	if (particleName == "gamma")
	{
	  //gamma
		//pManager->AddDiscreteProcess( new G4LowEnergyPhotoElectric() );
		//pManager->AddDiscreteProcess( new G4LowEnergyCompton() );
		//pManager->AddDiscreteProcess( new G4GammaConversion() );
		
		G4PhotoElectricEffect* thePhotoElectricEffect = new G4PhotoElectricEffect();
		G4LivermorePhotoElectricModel* theLivermorePhotoElectricModel = new G4LivermorePhotoElectricModel();
		thePhotoElectricEffect->SetModel(theLivermorePhotoElectricModel);
		pManager->AddDiscreteProcess(thePhotoElectricEffect);

		G4ComptonScattering* theComptonScattering = new G4ComptonScattering();
		G4LivermoreComptonModel* theLivermoreComptonModel = new G4LivermoreComptonModel();
		theComptonScattering->SetModel(theLivermoreComptonModel);
		pManager->AddDiscreteProcess(theComptonScattering);

		G4GammaConversion* theGammaConversion = new G4GammaConversion();
		G4LivermoreGammaConversionModel* theLivermoreGammaConversionModel = new G4LivermoreGammaConversionModel();
		theGammaConversion->SetModel(theLivermoreGammaConversionModel);
		pManager->AddDiscreteProcess(theGammaConversion);

		G4RayleighScattering* theRayleigh = new G4RayleighScattering();
		G4LivermoreRayleighModel* theRayleighModel = new G4LivermoreRayleighModel();
		theRayleigh->SetModel(theRayleighModel);
		pManager->AddDiscreteProcess(theRayleigh);


// 		G4Scintillation* m_Scintillation = new G4Scintillation();
// 		pManager->AddProcess( m_Scintillation );
// 		pManager->SetProcessOrderingToLast( m_Scintillation , idxAtRest);
// 		pManager->SetProcessOrderingToLast( m_Scintillation , idxPostStep);
	}
	else if (particleName == "e-")
	{
	  //electron
		pManager->AddProcess(new G4eMultipleScattering(),-1, 1,1);
		pManager->AddProcess(new G4eIonisation(),       -1, 2,2);
		pManager->AddProcess(new G4eBremsstrahlung(),   -1,-1,3);
	}
	else if (particleName == "e+")
	{
	  pManager->AddProcess(new G4eMultipleScattering(),-1, 1,1);
	  pManager->AddProcess(new G4eIonisation(),       -1, 2,2);
	  pManager->AddProcess(new G4eBremsstrahlung(),   -1,-1,3);
	  pManager->AddProcess(new G4eplusAnnihilation(),  0,-1,4);

 //		pManager->AddProcess( new G4Cerenkov() );
	}
	else if( (!particle->IsShortLived()) &&
	       (particle->GetPDGCharge() != 0.0) &&
	       (particle->GetParticleName() != "chargedgeantino") )
	{
	  //all others charged particles except geantino
		pManager->AddProcess(new G4eMultipleScattering(),-1,1,1);

		G4double demax = 0.05;  // try to lose at most 5% of the energy in
	  //    a single step (in limit of large energies)
		G4double stmin = 1.e-9 * m;  // length of the final step: 10 angstrom
	  // reproduced angular distribution of TRIM

		G4hLowEnergyIonisation* lowEIonisation = new G4hLowEnergyIonisation();
		pManager->AddProcess( lowEIonisation, -1,2,2);
		lowEIonisation->SetStepFunction( demax, stmin );
		}
	}
}

// Optical Processes ////////////////////////////////////////////////////////

void MTASPhysicsList::ConstructOp() 
{

  //G4double alphaMass= G4Alpha::Alpha()->GetPDGMass();
  //G4double neutronMass= G4Neutron::Neutron()->GetPDGMass();

  //GLG4Scint* theDefaultScintProcess = new GLG4Scint();
  //GLG4Scint* theNeutronScintProcess = new GLG4Scint("neutron", 0.9*neutronMass);
  //GLG4Scint* theAlphaScintProcess = new GLG4Scint("alpha", 0.9*alphaMass);

  // optical processes
  G4OpAbsorption* theAbsorptionProcess = new G4OpAbsorption();
  G4OpRayleigh* theRayleighScatteringProcess = new G4OpRayleigh();
  G4OpBoundaryProcess* theBoundaryProcess = new G4OpBoundaryProcess();

  theAbsorptionProcess->SetVerboseLevel(OpVerbLevel);
  theRayleighScatteringProcess->SetVerboseLevel(OpVerbLevel);
  theBoundaryProcess->SetVerboseLevel(OpVerbLevel);

  G4OpticalSurfaceModel themodel = unified;
  theBoundaryProcess->SetModel(themodel);

  theParticleIterator->reset();
  while( (*theParticleIterator)() )
  {
    G4ParticleDefinition* particle = theParticleIterator->value();
    G4ProcessManager* pmanager = particle->GetProcessManager();
    G4String particleName = particle->GetParticleName();

    if (particleName == "opticalphoton")
	{
		pmanager->AddDiscreteProcess(theAbsorptionProcess);
		pmanager->AddDiscreteProcess(theRayleighScatteringProcess);
		pmanager->AddDiscreteProcess(theBoundaryProcess);
    }
  }	
}




// Hadronic processes ////////////////////////////////////////////////////////

// Elastic processes:
#include "G4HadronElasticProcess.hh"

// Inelastic processes:
#include "G4ProtonInelasticProcess.hh"
#include "G4AntiProtonInelasticProcess.hh"
#include "G4NeutronInelasticProcess.hh"
#include "G4AntiNeutronInelasticProcess.hh"
#include "G4DeuteronInelasticProcess.hh"
#include "G4TritonInelasticProcess.hh"
#include "G4AlphaInelasticProcess.hh"

// Low-energy Models: < 20GeV
#include "G4LElastic.hh"
#include "G4LEProtonInelastic.hh"
#include "G4LEAntiProtonInelastic.hh"
#include "G4LENeutronInelastic.hh"
#include "G4LEAntiNeutronInelastic.hh"
#include "G4LEDeuteronInelastic.hh"
#include "G4LETritonInelastic.hh"
#include "G4LEAlphaInelastic.hh"

// High-energy Models: >20 GeV
#include "G4HEProtonInelastic.hh"
#include "G4HEAntiProtonInelastic.hh"
#include "G4HENeutronInelastic.hh"
#include "G4HEAntiNeutronInelastic.hh"

// Neutron high-precision models: <20 MeV
#include "G4NeutronHPElastic.hh"
#include "G4NeutronHPElasticData.hh"
#include "G4NeutronHPCapture.hh"
#include "G4NeutronHPCaptureData.hh"
#include "G4NeutronHPInelastic.hh"
#include "G4NeutronHPInelasticData.hh"

#include "G4LCapture.hh"
#include "G4HadronCaptureProcess.hh"

// Stopping processes
#include "G4AntiProtonAnnihilationAtRest.hh"
#include "G4AntiNeutronAnnihilationAtRest.hh"


void MTASPhysicsList::ConstructHad() 
{

  G4HadronElasticProcess* theElasticProcess = new G4HadronElasticProcess;
  G4LElastic* theElasticModel = new G4LElastic;
  theElasticProcess->RegisterMe(theElasticModel);

  theParticleIterator->reset();
  while ((*theParticleIterator)()) 
    {
      G4ParticleDefinition* particle = theParticleIterator->value();
      G4ProcessManager* pmanager = particle->GetProcessManager();
      G4String particleName = particle->GetParticleName();

      if (particleName == "proton") 
	{
	  pmanager->AddDiscreteProcess(theElasticProcess);
	  G4ProtonInelasticProcess* theInelasticProcess = 
	    new G4ProtonInelasticProcess("inelastic");
	  G4LEProtonInelastic* theLEInelasticModel = new G4LEProtonInelastic;
	  theInelasticProcess->RegisterMe(theLEInelasticModel);
	  G4HEProtonInelastic* theHEInelasticModel = new G4HEProtonInelastic;
	  theInelasticProcess->RegisterMe(theHEInelasticModel);
	  pmanager->AddDiscreteProcess(theInelasticProcess);
	  G4cout << "inside construct had 3 " << G4endl;
	}

      else if (particleName == "anti_proton") 
	{
	  pmanager->AddDiscreteProcess(theElasticProcess);
	  G4AntiProtonInelasticProcess* theInelasticProcess = 
	    new G4AntiProtonInelasticProcess("inelastic");
	  G4LEAntiProtonInelastic* theLEInelasticModel = 
	    new G4LEAntiProtonInelastic;
	  theInelasticProcess->RegisterMe(theLEInelasticModel);
	  G4HEAntiProtonInelastic* theHEInelasticModel = 
	    new G4HEAntiProtonInelastic;
	  theInelasticProcess->RegisterMe(theHEInelasticModel);
	  pmanager->AddDiscreteProcess(theInelasticProcess);
	}

      else if (particleName == "neutron") {
#define USE_HP_NUETRONPHYSICS
// elastic scattering
	G4HadronElasticProcess* theNeutronElasticProcess = new G4HadronElasticProcess;

#ifdef USE_HP_NUETRONPHYSICS
	G4NeutronHPElastic* theHPElasticNeutron = new G4NeutronHPElastic;
	theNeutronElasticProcess->RegisterMe(theHPElasticNeutron);
	
	G4NeutronHPElasticData* theHPNeutronData = new G4NeutronHPElasticData;
	theNeutronElasticProcess->AddDataSet(theHPNeutronData);
#else
	G4LElastic* theElasticModel1 = new G4LElastic;	
	theNeutronElasticProcess->RegisterMe(theElasticModel1);
//	theElasticModel1->SetMinEnergy(0.0*MeV);
#endif

	pmanager->AddDiscreteProcess(theNeutronElasticProcess);

// inelastic scattering
	G4NeutronInelasticProcess* theInelasticProcess = new G4NeutronInelasticProcess("inelastic");

#ifdef USE_HP_NUETRONPHYSICS
	G4NeutronHPInelastic* theLENeutronInelasticModel = new G4NeutronHPInelastic;
	theInelasticProcess->RegisterMe(theLENeutronInelasticModel);

	G4NeutronHPInelasticData* theNeutronData1 = new G4NeutronHPInelasticData;
	theInelasticProcess->AddDataSet(theNeutronData1);
#else
	G4LENeutronInelastic* theInelasticModel = new G4LENeutronInelastic;
	theInelasticProcess->RegisterMe(theInelasticModel);
#endif

	pmanager->AddDiscreteProcess(theInelasticProcess);
	
// capture
	G4HadronCaptureProcess* theCaptureProcess = new G4HadronCaptureProcess;

#ifdef USE_HP_NUETRONPHYSICS
	G4NeutronHPCapture* theLENeutronCaptureModel = new G4NeutronHPCapture;
	theCaptureProcess->RegisterMe(theLENeutronCaptureModel);

	G4NeutronHPCaptureData* theNeutronData3 = new G4NeutronHPCaptureData;
	theCaptureProcess->AddDataSet(theNeutronData3);
#else
	G4LCapture* theCaptureModel = new G4LCapture;
//	theCaptureModel->SetMinEnergy(0.0*MeV);// Default is 0.
	theCaptureProcess->RegisterMe(theCaptureModel);
#endif

	pmanager->AddDiscreteProcess(theCaptureProcess);

	//  G4ProcessManager* pmanager = G4Neutron::Neutron->GetProcessManager();
	//  pmanager->AddProcess(new G4UserSpecialCuts(),-1,-1,1);
      }
      else if (particleName == "anti_neutron") 
	{
	  pmanager->AddDiscreteProcess(theElasticProcess);
	  G4AntiNeutronInelasticProcess* theInelasticProcess = 
	    new G4AntiNeutronInelasticProcess("inelastic");

	  G4LEAntiNeutronInelastic* theLEInelasticModel = 
	    new G4LEAntiNeutronInelastic;
	  theInelasticProcess->RegisterMe(theLEInelasticModel);

	  G4HEAntiNeutronInelastic* theHEInelasticModel = 
	    new G4HEAntiNeutronInelastic;
	  theInelasticProcess->RegisterMe(theHEInelasticModel);

	  pmanager->AddDiscreteProcess(theInelasticProcess);
	}

      else if (particleName == "deuteron") 
	{
	    G4cout << "inside construct had 4 " << G4endl;
	  pmanager->AddDiscreteProcess(theElasticProcess);
	  G4DeuteronInelasticProcess* theInelasticProcess = 
	    new G4DeuteronInelasticProcess("inelastic");
	  G4LEDeuteronInelastic* theLEInelasticModel = 
	    new G4LEDeuteronInelastic;
	  theInelasticProcess->RegisterMe(theLEInelasticModel);
	  pmanager->AddDiscreteProcess(theInelasticProcess);
	}
      
      else if (particleName == "triton") 
	{
	  pmanager->AddDiscreteProcess(theElasticProcess);
	  G4TritonInelasticProcess* theInelasticProcess = 
	    new G4TritonInelasticProcess("inelastic");
	  G4LETritonInelastic* theLEInelasticModel = 
	    new G4LETritonInelastic;
	  theInelasticProcess->RegisterMe(theLEInelasticModel);
	  pmanager->AddDiscreteProcess(theInelasticProcess);
	}

      else if (particleName == "alpha") 
	{
	  pmanager->AddDiscreteProcess(theElasticProcess);
	  G4AlphaInelasticProcess* theInelasticProcess = 
	    new G4AlphaInelasticProcess("inelastic");
	  G4LEAlphaInelastic* theLEInelasticModel = 
	    new G4LEAlphaInelastic;
	  theInelasticProcess->RegisterMe(theLEInelasticModel);
	  pmanager->AddDiscreteProcess(theInelasticProcess);
	}

    }
}

//Processes

void MTASPhysicsList::ConstructGeneral()
{
  G4Decay* theDecayProcess = new G4Decay();
  theParticleIterator->reset();

  while( (*theParticleIterator)() ){
    G4ParticleDefinition* particle = theParticleIterator->value();
    G4ProcessManager* pmanager = particle->GetProcessManager();
    if (theDecayProcess->IsApplicable(*particle)) {
	pmanager ->AddProcess(theDecayProcess);
	pmanager ->SetProcessOrdering(theDecayProcess, idxPostStep);
	pmanager ->SetProcessOrdering(theDecayProcess, idxAtRest);
    }
  }
}
